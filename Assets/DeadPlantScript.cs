﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadPlantScript : MonoBehaviour
{
    private float vanishSpeed = 0.1f;

    private SpriteRenderer currentSpriteRenderer;

    void Find()
    {
        GameObject currentChild;
        for (int i = 0; i < transform.childCount; i++)
        {
            currentChild = transform.GetChild(i).gameObject;
            if (currentChild.activeSelf)
            {
                Debug.Log(currentChild);
            }
            if (currentChild.activeSelf && currentChild.GetComponent<SpriteRenderer>())
            {
                currentSpriteRenderer = currentChild.GetComponent<SpriteRenderer>();
                break;
            }
        }
    }

    public void TriggerVanish()
    {
        Find();
        Debug.Log("Vanish");
        StartCoroutine(FadeTo(0, 1f));
    }

    public IEnumerator FadeTo(float alpha, float time = 0.5f)
    {
        Debug.Log(currentSpriteRenderer);

        var color = currentSpriteRenderer.color;
        var startAlpha = color.a;
        var elapsed = 0.0f;

        while (elapsed < time)
        {
            color.a = Mathf.Lerp(startAlpha, alpha, elapsed / time);
            currentSpriteRenderer.color = color;
            elapsed += Time.unscaledDeltaTime;
            yield return null;
        }

        color.a = alpha;
        currentSpriteRenderer.color = color;
        Death();
    }

    private void Death()
    {
        Destroy(gameObject);
    }
}
