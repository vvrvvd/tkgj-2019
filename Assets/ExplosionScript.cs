﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour
{
    private BeetrootBehaviour beetrootBehaviour;

    private void Awake()
    {
        beetrootBehaviour = transform.GetComponentInParent<BeetrootBehaviour>();
    }

    public void TriggerExplosion()
    {
        beetrootBehaviour.ExplosionAnimation();
    }

    public void TriggerDeath()
    {
        Destroy(beetrootBehaviour.gameObject);
    }
}
