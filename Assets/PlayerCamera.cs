﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    scr_Player player;
    public float minX, maxX, minY, maxY;
    public float smoothTime;
    public float smoothTimeModifier = 0.4f;
    private Vector3 depth;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<scr_Player>();
        depth = new Vector3(0, 0, transform.position.z);
    }

    private void FixedUpdate()
    {
        MoveCamera();
        correctPosition();
    }

    private void MoveCamera()
    {
        if (player.isWalking == false)
            transform.position = Vector3.Lerp(transform.position, player.transform.position + depth, smoothTime * 0.4f);
        else
            transform.position = Vector3.Lerp(transform.position, player.transform.position + depth, smoothTime);
    }

    private void correctPosition()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minX, maxX), Mathf.Clamp(transform.position.y, minY, maxY), 0f) + depth;
    }
}
