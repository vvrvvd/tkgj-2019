﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeetrootBehaviour : PlantBehaviour
{
    public int damage = 20;
    public bool detonated;

    private GameObject loadingObject;
    private GameObject explosionObject;

    private List<EnemyStats> enemiesInRange;

    protected override void Awake()
    {
        base.Awake();
        enemiesInRange = new List<EnemyStats>();
        loadingObject = transform.GetChild(4).gameObject;
        explosionObject = transform.GetChild(5).gameObject;
    }

    protected override void Attack()
    {
        base.Attack();
        if (detonated || enemiesInRange.Count==0) return;
        if (CurrentStadium.Equals(PlantStadiums.Mutated)) Detonate();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            
            var destruct = collision.GetComponent<EnemyStats>();
            if (destruct)
            {
                Debug.Log(collision.gameObject.name);
                enemiesInRange.Add(destruct);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            var destruct = collision.GetComponent<EnemyStats>();
            if (destruct)
            {
                enemiesInRange.Remove(destruct);
            }
        }
    }

    private void Detonate()
    {
        detonated = true;
        for (int i = 0; i < 4; i++)
            PlantObjects[i].SetActive(false);
        loadingObject.SetActive(true);
    }

    public void ExplosionAnimation()
    {
        foreach (var destruct in enemiesInRange)
        {
            if (destruct) destruct.GiveDamage(damage);
        }

        loadingObject.SetActive(false);
        explosionObject.SetActive(true);
    }
}
