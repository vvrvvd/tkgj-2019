﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] private int damage = 6;
    [SerializeField] private float speed = 2;

    public EnemyStats target;

    private void Update()
    {
        if (target)
        {
            transform.position += (target.transform.position - transform.position).normalized * Time.deltaTime * speed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (target && target == collision.GetComponent<EnemyStats>())
        {
            collision.GetComponent<EnemyStats>().GiveDamage(damage);
            Destroy(gameObject);
        }    
    }

}
