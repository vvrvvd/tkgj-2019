﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour
{

    private Animator animator;
    private ParticleSystem walkParticles;

    void Awake()
    {
        animator = GetComponent<Animator>();
        walkParticles = GetComponentInChildren<ParticleSystem>();
    }

    public void StartWalking()
    {
        animator.SetBool("isWalking", true);
    }

    public void StopWalking()
    {
        animator.SetBool("isWalking", false);
    }

    public void TakeBarrel()
    {
        animator.SetBool("isHoldingBarrel", true);
    }

    public void PutBarrel()
    {
        animator.SetBool("isHoldingBarrel", false);
    }

    public void EmitWalkParticle()
    {
        var count = Random.Range(2, 6);
        walkParticles.Emit(count);
    }

    public void Empty()
    {

    }
}
