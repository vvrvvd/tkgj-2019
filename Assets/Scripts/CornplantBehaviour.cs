﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornplantBehaviour : PlantBehaviour
{
    //strzela pociskami
    [SerializeField] private int damage = 4;
    [SerializeField] private float reloadTime = 2f;
    [SerializeField] private Transform facePivot;
    [SerializeField] private BulletScript bulletPrefab;

    private List<EnemyStats> enemiesInRange;
    private EnemyStats nearestEnemy;

    private float previousReloadTime = 0;

    protected override void Awake()
    {
        base.Awake();
        enemiesInRange = new List<EnemyStats>();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        RemoveDestroyedEnemiesFromList();
        if (!nearestEnemy) SetNearestEnemy();    
    }

    protected override void Attack()
    {
        if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            if (nearestEnemy)
            {
                //nearestEnemy.GiveDamage(damage);
                if(Time.time - previousReloadTime >= reloadTime)
                {
                    BulletScript bullet = Instantiate(bulletPrefab, facePivot.position, facePivot.rotation);
                    bullet.target = nearestEnemy;
                    previousReloadTime = Time.time;
                }
            }
        }
    }

    private void SetNearestEnemy()
    {

        float nearestDistance = -1;
        int indexOfNearest = -1;
        for (int i = 0; i < enemiesInRange.Count; i++)
        {
            float magnitude = (transform.position - enemiesInRange[i].transform.position).magnitude;
            if (i == 0)
            {
                nearestDistance = magnitude;
                indexOfNearest = 0;
            }
            else
            {
                if (magnitude < nearestDistance)
                {
                    nearestDistance = magnitude;
                    indexOfNearest = i;
                }
            }

        }
        if (indexOfNearest == -1)
            nearestEnemy = null;
        else
            nearestEnemy = enemiesInRange[indexOfNearest];
    }

    private void RemoveDestroyedEnemiesFromList()
    {
        for (int i = 0; i < enemiesInRange.Count; i++)
        {
            if (enemiesInRange[i] == null)
                enemiesInRange.Remove(enemiesInRange[i]);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            var destruct = collision.GetComponent<EnemyStats>();
            if (destruct)
            {
                enemiesInRange.Add(destruct);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            var destruct = collision.GetComponent<EnemyStats>();
            if (destruct)
            {
                enemiesInRange.Remove(destruct);
                if (destruct == nearestEnemy)
                {
                    nearestEnemy = null;
                }
            }
        }
    }
}
