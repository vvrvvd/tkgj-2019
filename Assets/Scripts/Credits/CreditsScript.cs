﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;

public class CreditsScript : MonoBehaviour
{

    private SceneFader fader;
    private bool isFading;

    void Awake()
    {
        fader = GameObject.FindObjectOfType<SceneFader>();
    }

    void Start()
    {
        BackgroundStats.BackgroundSpeed = 2.0f;
    }

    public void TransitionToMainMenu()
    {
        if(!isFading)
        {
            fader.LoadScene("MenuScene", 2.0f);
            isFading = true;
        }
    }

}
