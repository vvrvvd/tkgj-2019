﻿using UnityEngine;
using UnityEngine.Events;

public abstract class Destroyable : MonoBehaviour
{
    [SerializeField] protected int health;
    [HideInInspector] public UnityEvent Death;
    public bool IsDead => health <= 0;

    private void Awake()
    {
        Death = new UnityEvent();
    }

    public void GiveDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
            Death.Invoke();
    }
}