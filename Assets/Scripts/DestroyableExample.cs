﻿public class DestroyableExample : Destroyable
{
    private void OnEnable()
    {
        Death.AddListener(OnDeath);
    }

    private void OnDisable()
    {
        Death.RemoveListener(OnDeath);
    }

    public void OnDeath()
    {
        Destroy(gameObject);
    }
}