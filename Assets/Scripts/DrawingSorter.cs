﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawingSorter : MonoBehaviour
{
    public SpriteRenderer[] sprRenderer;

    public void Awake()
    {
        sprRenderer = GetComponentsInChildren<SpriteRenderer>(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
