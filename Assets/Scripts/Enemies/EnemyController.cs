﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(EnemyStats))]
public class EnemyController : MonoBehaviour
{
    [SerializeField] private EnemyStats stats;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private SpriteRenderer sprite;
    private Destroyable target;
    private bool isWaiting = false;

    private Coroutine attackCoroutine;
    private Coroutine startCoroutine;

    [SerializeField] private bool facingRight = false;

    // Start is called before the first frame update
    void Start()
    {
        target = AcquireTarget();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(agent.transform.position.x, agent.transform.position.y, transform.position.z);
        if (target == null)
            target = AcquireTarget();
        if(target==null)
            return;
        if (target.transform.position.x > transform.position.x && !facingRight || target.transform.position.x < transform.position.x && facingRight)
            sprite.flipX = true;
        else
            sprite.flipX = false;
        if (IsInAttackRange(target.transform.position))
        {
            StopMovement();
            if (attackCoroutine == null)
                attackCoroutine = StartCoroutine(Attack());
        }
        else
        {
            if(attackCoroutine != null)
                StopCoroutine(attackCoroutine);
            MoveToTarget(target.transform.position);
        }
    }

    private void OnEnable()
    {
        stats.Death.AddListener(OnDeath);
    }

    private void OnDisable()
    {
        stats.Death.RemoveListener(OnDeath);
    }

    private void OnValidate()
    {
        stats = GetComponent<EnemyStats>();
        agent.speed = stats.speed;
    }

    private Destroyable AcquireTarget()
    {
        Destroyable[] potentialTargets = FindObjectsOfType<Destroyable>().Where(x => !(x is EnemyStats)).
            OrderBy(x => Vector2.Distance(transform.position, x.transform.position)).ToArray();
        for (int i = 0; i < potentialTargets.Length; i++)
        {
            var potentialTarget = potentialTargets[i];
            if (!potentialTarget.IsDead)
            {
                potentialTarget.Death.AddListener(OnTargetDeath);
                return potentialTarget;
            }
        }
        return null;
    }

    private void MoveToTarget(Vector2 targetPosition)
    {
        agent.destination = targetPosition;
    }

    private void StopMovement()
    {
        agent.destination = transform.position;
    }

    private bool IsInAttackRange(Vector2 targetPosition)
    {
        return Vector2.Distance(transform.position, targetPosition) <= stats.range;
    }

    private IEnumerator Attack()
    {
        //Debug.Log($"{name} IsAttacking");
        yield return new WaitForSeconds(1f);
        if(target != null && !target.IsDead)
            target.GiveDamage(stats.damage);
        attackCoroutine = null;
    }

    public void OnTargetDeath()
    {
        target = AcquireTarget();
    }

    public void OnDeath()
    {
        //play death animation/spawn particles/give money
        Destroy(transform.root.gameObject);
    }

    public void StopAgent()
    {
        agent.isStopped = true;
        if (startCoroutine != null)
            StopCoroutine(startCoroutine);
    }

    public void StopAgent(float time)
    {
        agent.isStopped = true;
        if (startCoroutine != null)
            StopCoroutine(startCoroutine);
        startCoroutine = StartCoroutine(StartAfterTimePasses(time));
    }

    private IEnumerator StartAfterTimePasses(float time)
    {
        yield return new WaitForSeconds(time);
        agent.isStopped = false;
    }

    public void StartAgent()
    {
        agent.isStopped = false;
    }
}
