﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class EnemySpawner : MonoBehaviour
{
    private static readonly float instantiateInterwal = 1f;

    private static int spawnedEnemiesCount;
    [HideInInspector] public static UnityEvent AllEnemiesDied = new UnityEvent();

    public void SpawnEnemies(GameObject enemy, int numberOfEnemies)
    {
        StartCoroutine(SpawnEnemiesCoroutine(enemy, numberOfEnemies));
    }

    private IEnumerator SpawnEnemiesCoroutine(GameObject enemy, int numberOfEnemies)
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            GameObject instance = Instantiate(enemy, transform);
            spawnedEnemiesCount++;
            instance.GetComponentInChildren<EnemyStats>().Death.AddListener(OnEnemyDeath);
            yield return new WaitForSeconds(instantiateInterwal);
        }
    }

    private void OnEnemyDeath()
    {
        spawnedEnemiesCount--;
        if (spawnedEnemiesCount == 0)
            AllEnemiesDied.Invoke();
    }
}
