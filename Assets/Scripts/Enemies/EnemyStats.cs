﻿using UnityEngine;

public class EnemyStats : Destroyable
{
    public float range = 1f;

    public int damage;
    public float speed;
    [SerializeField] private int spawnCost;
    public int SpawnCost => spawnCost;
    [SerializeField] private Transform targetPoint;
    public Vector2 TargetPoint => targetPoint.position;
}
