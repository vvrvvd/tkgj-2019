﻿using UnityEngine;
using UnityEngine.AI;

public class TractorWheelsController : MonoBehaviour
{
    private static readonly float rotationSpeed = 360f;

    [SerializeField] private SpriteRenderer tractorSprite;
    [SerializeField] private NavMeshAgent agent;

    [SerializeField] private SpriteRenderer backWheel;
    [SerializeField] private SpriteRenderer frontWheel;

    private float xOffsetBack;
    private float xOffsetFront;

    private void Start()
    {
        xOffsetBack = backWheel.transform.localPosition.x;
        xOffsetFront = frontWheel.transform.localPosition.x;
    }

    // Update is called once per frame
    void Update()
    {
        if(agent.velocity.magnitude > 0.1f)
        {
            float localRotation = tractorSprite.flipX == true ? rotationSpeed : -rotationSpeed;
            backWheel.transform.Rotate(0f, 0f, Time.deltaTime * localRotation);
            frontWheel.transform.Rotate(0f, 0f, Time.deltaTime * localRotation);
        }
        if(tractorSprite.flipX == true)
        {
            backWheel.transform.localPosition = new Vector3(-xOffsetBack, backWheel.transform.localPosition.y, backWheel.transform.localPosition.z);
            frontWheel.transform.localPosition = new Vector3(-xOffsetFront, frontWheel.transform.localPosition.y, frontWheel.transform.localPosition.z);
        }
        else
        {
            backWheel.transform.localPosition = new Vector3(xOffsetBack, backWheel.transform.localPosition.y, backWheel.transform.localPosition.z);
            frontWheel.transform.localPosition = new Vector3(xOffsetFront, frontWheel.transform.localPosition.y, frontWheel.transform.localPosition.z);
        }
    }

    void LateUpdate()
    {
        backWheel.sortingOrder = transform.parent.GetComponent<SpriteRenderer>().sortingOrder + 1;
        frontWheel.sortingOrder = transform.parent.GetComponent<SpriteRenderer>().sortingOrder + 1;
    }
}
