﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;

public class CharacterIntroController : MonoBehaviour
{
    [SerializeField]
    private GameObject barrelObject;

    [SerializeField]
    private CharacterAnimator characterAnimator;

    [SerializeField]
    private Animator sceneAnimator;

    [SerializeField]
    private float transitionTime;

    private SceneFader fader;

    void Awake()
    {
        fader = GameObject.FindObjectOfType<SceneFader>();
        sceneAnimator = GetComponent<Animator>();
    }

    public void StartWalking()
    {
        characterAnimator.StartWalking();
    }

    public void StopWalking()
    {
        characterAnimator.StopWalking();
    }

    public void TakeBarrel()
    {
        if(barrelObject!=null)
            barrelObject.SetActive(false);
        characterAnimator.TakeBarrel();
    }

    public void PutBarrel()
    {
        if (barrelObject != null)
        {
            Vector3 pos = characterAnimator.transform.position;
            pos.y = pos.y - 1.2f;
            barrelObject.transform.position = pos;
            barrelObject.SetActive(true);
        }
        characterAnimator.PutBarrel();
    }

    public void TransitionToMainMenu()
    {
        fader.LoadScene("MenuScene", transitionTime);
    }

    public void TransitionToPlayScene()
    {
        fader.LoadScene("MenuScene", transitionTime);
    }

    public void TransitionToCreditsScene()
    {
        fader.LoadScene("MenuScene", transitionTime);
    }

    bool isTransition = false;

    public void PlayGameAnimation()
    {
        if(!isTransition)
        {
            isTransition = true;
            sceneAnimator.SetTrigger("playGame");
        }
    }

    public void ExitGameAnimation()
    {
        if (!isTransition)
        {
            isTransition = true;
            sceneAnimator.SetTrigger("exitGame");
        }
    }

    public void CreditsAnimation()
    {
        if (!isTransition)
        {
            isTransition = true;
            fader.LoadScene("CreditsScene", transitionTime);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
