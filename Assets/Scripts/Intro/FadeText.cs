﻿using System;
using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class FadeText : MonoBehaviour
{

    [SerializeField] private bool isFadingInOnStart;
    [SerializeField] private float fadeInTime;
    [SerializeField] private float stopTimeOnStart;
    [SerializeField] private bool isFadingOutOnStart;
    [SerializeField] private float fadeOutTime;
    [SerializeField] private UnityEvent onFadeInFinished;
    [SerializeField] private UnityEvent onFadeOutFinished;

    private float currentAlpha;
    private float setAlpha;
    private bool isFadingIn;
    private TextMeshProUGUI text;
    private Action onFadeFinished;
    private IEnumerator fadeCoroutine;
    private float stopTime;

    void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Start()
    {
        if (isFadingInOnStart && isFadingOutOnStart)
            FadeInAndOut(fadeInTime, fadeOutTime, stopTimeOnStart, onFadeInFinished, onFadeOutFinished);
        else if (isFadingInOnStart)
            FadeIn(fadeInTime, () => onFadeInFinished.Invoke());
        else if (isFadingOutOnStart)
            FadeOut(fadeOutTime, () => onFadeOutFinished.Invoke());
    }

    public void FadeInAndOut(float fadeInTime, float fadeOutTime, float stopTime = 0.0f, UnityEvent onFadeInFinished = null, UnityEvent onFadeOutFinished = null)
    {
        this.stopTime = 0.0f;
        FadeIn(fadeInTime, () => { if (onFadeInFinished != null) onFadeInFinished.Invoke(); this.stopTime = stopTime; FadeOut(fadeOutTime, () => { if (onFadeOutFinished != null) onFadeOutFinished.Invoke(); }); });
    }

    public void FadeIn(float time, Action onFadeFinished = null)
    {
        isFadingIn = true;
        ResetFade(0.0f, 1.0f);
        StartFade(time, onFadeFinished);
    }

    public void FadeOut(float time, Action onFadeFinished = null)
    {
        isFadingIn = false;
        ResetFade(1.0f, 0.0f);
        StartFade(time, onFadeFinished);
    }

    private void ResetFade(float currentAlpha, float setAlpha)
    {
        if (fadeCoroutine != null)
            StopCoroutine(fadeCoroutine);

        this.currentAlpha = currentAlpha;
        this.setAlpha = setAlpha;

        text.color = new Color(text.color.r, text.color.g, text.color.b, currentAlpha);
    }

    private void StartFade(float time, Action onFadeFinished)
    {
        this.onFadeFinished = onFadeFinished;
        fadeCoroutine = FadeCoroutine(time);
        StartCoroutine(fadeCoroutine);
    }

    private IEnumerator FadeCoroutine(float time)
    {
        yield return new WaitForSeconds(stopTime);

        float deltaTime = 0.0f;
        while(deltaTime < time)
        {
            yield return new WaitForEndOfFrame();
            deltaTime += Time.deltaTime;
            currentAlpha = Mathf.Min(deltaTime / time, 1.0f);
            currentAlpha = isFadingIn ? currentAlpha : 1.0f - currentAlpha;
            text.color = new Color(text.color.r, text.color.g, text.color.b, currentAlpha);
        }

        text.color = new Color(text.color.r, text.color.g, text.color.b, setAlpha);

        if (onFadeFinished != null)
            onFadeFinished.Invoke();
    }

}
