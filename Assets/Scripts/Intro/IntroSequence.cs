﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntroSequence : MonoBehaviour
{

    [SerializeField]
    private UnityEvent startEvent;

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();

        if(startEvent!=null)
            startEvent.Invoke();
    }

}
