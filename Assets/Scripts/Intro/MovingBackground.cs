﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MovingBackground : MonoBehaviour
{
    [SerializeField]
    private float minX;

    [SerializeField]
    private float maxX;

    [SerializeField]
    private float speed;

    private Transform[] children;

    void Awake()
    {
        children = GetComponentsInChildren<Transform>().Where(go => go.gameObject != this.gameObject).ToArray();
    }

    void Update()
    {
        for(int i=0; i<children.Length; i++)
        {
            children[i].Translate(speed * BackgroundStats.BackgroundSpeed* Time.deltaTime, 0.0f, 0.0f);
            if(children[i].localPosition.x <= minX)
            {
                float diff = children[i].localPosition.x - minX;
                children[i].localPosition = new Vector3(maxX + diff, children[i].localPosition.y, children[i].localPosition.z);
            } else if (children[i].localPosition.x >= maxX)
            {
                float diff = children[i].localPosition.x - maxX;
                children[i].localPosition = new Vector3(minX + diff, children[i].localPosition.y, children[i].localPosition.z);
            }
        }
    }
}
