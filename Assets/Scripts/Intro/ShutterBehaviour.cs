﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class ShutterBehaviour : MonoBehaviour
{
    enum Position {  Up, Down };

    public float AnimationTime;
    public UnityEvent OnAnimationFinished;

    [SerializeField]
    private Position position;

    [SerializeField]
    private bool isOpened;

    private float startPosition;

    private SpriteRenderer spriteRenderer;
    private IEnumerator animationCoroutine;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        ResizeSpriteToScreen();

        if (isOpened)
            transform.position = new Vector3(transform.position.x, transform.position.y * 3.0f, transform.position.z);
    }

    private void ResizeSpriteToScreen()
    {
        if (spriteRenderer == null) return;

        transform.localScale = new Vector3(1, 1, 1);

        var width = spriteRenderer.sprite.bounds.size.x;
        var height = spriteRenderer.sprite.bounds.size.y;

        var worldScreenHeight = Camera.main.orthographicSize * 2.0;
        var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        var scale = Vector3.one;
        scale.x = (float) worldScreenWidth / width;
        scale.y = (float)(worldScreenHeight / 2.0f) / height;
        transform.localScale = scale;

        var pos = Vector3.zero;
        pos.y = (float)worldScreenHeight / 4.0f;

        if (position == Position.Down)
            pos.y *= -1;

        startPosition = pos.y;
        transform.position = pos;
    }

    public void Open()
    {
        if (animationCoroutine != null)
            StopCoroutine(animationCoroutine);

        transform.position = new Vector3(transform.position.x, startPosition, transform.position.z);
        animationCoroutine = Animate(startPosition * 3.0f);
        StartCoroutine(animationCoroutine);
    }

    public void Close()
    {
        if (animationCoroutine != null)
            StopCoroutine(animationCoroutine);

        transform.position = new Vector3(transform.position.x, startPosition * 3.0f, transform.position.z);
        animationCoroutine = Animate(startPosition);
        StartCoroutine(animationCoroutine);
    }

    public IEnumerator Animate(float setPosition)
    {
        float deltaTime = 0.0f;

        while (deltaTime < AnimationTime)
        {
            yield return new WaitForEndOfFrame();
            deltaTime += Time.deltaTime;
            float currPos = startPosition + (deltaTime / AnimationTime) * (setPosition - startPosition);
            if(position==Position.Up)
                currPos = Mathf.Min(currPos, setPosition);
            else
                currPos = Mathf.Max(currPos, setPosition);
            transform.position = new Vector3(transform.position.x, currPos, transform.position.z);
        }

        transform.position = new Vector3(transform.position.x, setPosition, transform.position.z);
        if (OnAnimationFinished != null)
            OnAnimationFinished.Invoke();
    }
}
