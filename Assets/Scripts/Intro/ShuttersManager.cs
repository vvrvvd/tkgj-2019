﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShuttersManager : MonoBehaviour
{
    public UnityEvent OnAnimationFinished;

    private ShutterBehaviour[] shutters;

    void Awake()
    {
        shutters = GetComponentsInChildren<ShutterBehaviour>();
    }

    public void Open()
    {
        shutters[0].OnAnimationFinished = OnAnimationFinished;

        foreach (ShutterBehaviour sh in shutters)
            sh.Open();
    }

    public void Close()
    {
        shutters[0].OnAnimationFinished = OnAnimationFinished;

        foreach (ShutterBehaviour sh in shutters)
            sh.Open();
    }

}
