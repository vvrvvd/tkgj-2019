﻿using UnityEngine;
using UnityEngine.Events;

public class TruckBehaviour : MonoBehaviour
{
    [SerializeField] private UnityEvent OnAnimationFinishedEvent;

    private Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void StartIntro()
    {
        animator.SetTrigger("StartIntro");
    }

    public void OnAnimationFinished()
    {
        OnAnimationFinishedEvent.Invoke();
    }

    public void StopBackground()
    {
        BackgroundStats.BackgroundSpeed = 0.0f;
    }

    public void SetBackgroundSpeed(float speed)
    {
        BackgroundStats.BackgroundSpeed = speed;
    }

}
