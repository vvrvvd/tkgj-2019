﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelBehaviour : MonoBehaviour
{
    [SerializeField]
    private float angularSpeed;

    void Update()
    {
        transform.Rotate(0.0f, 0.0f, angularSpeed * Time.deltaTime);
    }
}
