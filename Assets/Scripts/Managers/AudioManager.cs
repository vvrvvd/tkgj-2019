﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource clip;

    private static AudioManager instance;

    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AudioManager>();
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if (this != instance)
                Destroy(gameObject);
        }
    }

    private void Start()
    {
        clip.Play();
    }

    private void Update()
    {
        if (Camera.main != null)
        {
            instance.transform.position = Camera.main.transform.position;
        }
        if (clip.isPlaying || clip.time != 0f)
            return;
        clip.Play();
    }
}