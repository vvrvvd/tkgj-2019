﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Game
{
    [RequireComponent(typeof(GameManager))]
    public class CanvasManager : MonoBehaviour
    {
        private GameManager GameManager;

        public TextMeshProUGUI GoldText;
        public TextMeshProUGUI TimeText;

        private void Awake()
        {
            #region Self-Injection

            GameManager = GetComponent<GameManager>();

            #endregion
        }

        private void Update()
        {
            GoldText?.SetText(GameManager.Gold.ToString());
            TimeText?.SetText(GameManager.Time.ToString());
        }
    }
}