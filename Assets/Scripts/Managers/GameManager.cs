﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class GameManager : MonoBehaviour
    {
        #region Fields

        public int gold;
        public int goldIncome;
        public int allTimeGold;

        [Space]

        public float time;
        public float interval;

        #endregion

        private void Start()
        {
            StartCoroutine(Clock());

            allTimeGold = gold;
        }

        private void OnValidate()
        {
            gold = Mathf.Max(0, gold);
            time = Mathf.Max(0, time);

            interval = Mathf.Max(0.1f, interval);
            goldIncome = Mathf.Max(0, goldIncome);
        }

        private IEnumerator Clock()
        {
            var waiter = new WaitForSeconds(interval);
            while(true)
            {
                yield return waiter;
                time += interval;
                GiveGold(goldIncome);
            }
        }

        public void GiveGold(int goldIncome)
        {
            gold += goldIncome;
            gold = Mathf.Max(0, gold);
            allTimeGold += gold;
        }

        public bool UseGold(int payment)
        {
            if (gold >= payment)
            {
                gold -= payment;
                return true;
            }

            return false;
        }

        #region Properties

        public int Gold => gold;
        public float Time => time;

        #endregion
    }
}