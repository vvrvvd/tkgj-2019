﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public interface IUserInput
    {
        #region Keys methods

        bool Key(KeyCode code);
        bool KeyUp(KeyCode code);
        bool KeyDown(KeyCode code);

        #endregion

        #region Mouse methods

        bool MouseButton(int mouseButton);
        bool MouseButtonUp(int mouseButton);
        bool MouseButtonDown(int mouseButton);

        float GetAxis(string axisName);

        #endregion

        #region Properties

        bool AnyKey { get; }
        Vector3 MousePos { get; }

        #endregion
    }

    public class UnityInput : IUserInput
    {
        #region Public methods

        public bool Key(KeyCode code)
        {
            return Input.GetKey(code);
        }

        public bool KeyUp(KeyCode code)
        {
            return Input.GetKeyUp(code);
        }

        public bool KeyDown(KeyCode code)
        {
            return Input.GetKeyDown(code);
        }

        public bool MouseButton(int mouseButton)
        {
            return Input.GetMouseButton(mouseButton);
        }

        public bool MouseButtonUp(int mouseButton)
        {
            return Input.GetMouseButtonUp(mouseButton);
        }

        public bool MouseButtonDown(int mouseButton)
        {
            return Input.GetMouseButtonDown(mouseButton);
        }

        public float GetAxis(string axisName)
        {
            return Input.GetAxis(axisName);
        }

        #endregion

        #region Properties

        public bool AnyKey => Input.anyKey;
        public Vector3 MousePos => Input.mousePosition;

        #endregion
    }

    public class TestInput : IUserInput
    {
        #region Public methods

        public bool Key(KeyCode code)
        {
            return Input.GetKey(code);
        }

        public bool KeyDown(KeyCode code)
        {
            return Input.GetKeyDown(code);
        }

        public bool KeyUp(KeyCode code)
        {
            return Input.GetKeyUp(code);
        }

        public bool MouseButton(int mouseButton)
        {
            return Input.GetMouseButton(mouseButton);
        }

        public bool MouseButtonUp(int mouseButton)
        {
            return Input.GetMouseButtonUp(mouseButton);
        }

        public bool MouseButtonDown(int mouseButton)
        {
            return Input.GetMouseButtonDown(mouseButton);
        }

        public float GetAxis(string axisName)
        {
            return Input.GetAxis(axisName);
        }

        #endregion

        #region Properties

        public bool AnyKey => Input.anyKey;
        public Vector3 MousePos => Input.mousePosition;

        #endregion
    }
}