﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    public interface IKeyHandler
    {
        void AddListener(KeyCode key, UnityAction listener);
        void RemoveListener(KeyCode key, UnityAction listener);
    }

    public class KeyManager : MonoBehaviour, IKeyHandler
    {
        [Serializable]
        public struct Shortcout
        {
            public KeyCode keyCode;
            public UnityEvent keyEvent;
        }

        #region Private fields 

        private IUserInput userInput;

        private Dictionary<KeyCode, UnityEvent> KeyEvents;

        #endregion

        #region Public fields 

        public UnityEvent anyKeyEvent;

        [Space]

        [ArrayElementTitle("keyCode")]
        public List<Shortcout> shortcouts;

        #endregion

        #region Init methods

        private void Awake()
        {
            #region Self-Injection

            userInput = new UnityInput();

            #endregion

            KeyEvents = new Dictionary<KeyCode, UnityEvent>();

            foreach (var sc in shortcouts)
            {
                AddListener(sc.keyCode, sc.keyEvent.Invoke);
            }
        }

        private void Start()
        {
            if (shortcouts == null) shortcouts = new List<Shortcout>();
        }

        #endregion

        #region Loop methods

        private void Update()
        {
            if (userInput.AnyKey) anyKeyEvent?.Invoke(); 

            foreach (var entry in KeyEvents)
            {
                if (userInput.KeyDown(entry.Key)) entry.Value?.Invoke();           
            }
        }

        #endregion

        #region Public methods 

        public void AddListener(KeyCode key, UnityAction listener)
        {
            if (KeyEvents.ContainsKey(key))
            {
                KeyEvents[key].AddListener(listener);
            }
            else
            {
                UnityEvent e = new UnityEvent();
                e.AddListener(listener);
                KeyEvents.Add(key, e);
            }
        }

        public void RemoveListener(KeyCode key, UnityAction listener)
        {
            if (KeyEvents.ContainsKey(key))
            {
                KeyEvents[key].RemoveListener(listener);
            }
            else
            {
#if UNITY_EDITOR
                UnityEngine.Debug.LogWarning("Wrong key", this);
#endif
            }
        }

        #endregion
    }
}