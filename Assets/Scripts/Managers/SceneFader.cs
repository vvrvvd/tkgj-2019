﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Game
{
    [RequireComponent(typeof(Canvas))]
    [RequireComponent(typeof(Image))]
    public class SceneFader : MonoBehaviour
    {
        [SerializeField]
        private bool sceneChanging;
        private float lastTransitionTime;

        private Canvas Canvas;
        private Image Image;

        #region Init and deinit methods

        private void Awake()
        {
            #region Self-Injection 

            Canvas = GetComponent<Canvas>();
            Image = GetComponent<Image>();
        
            #endregion
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);

            Canvas.sortingOrder = System.Int16.MaxValue;
        }

        private void OnEnable()
        {
            UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnLevelFinishedLoading;
        }

        private void OnDisable()
        {
            UnityEngine.SceneManagement.SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        }

        #endregion

        private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
        {
            StartCoroutine(Painter.FadeTo(Image, 0, lastTransitionTime / 2));
        }

        private IEnumerator LoadInTime(string sceneName, float time)
        {
            if (sceneChanging) yield break;

            sceneChanging = true;
            yield return Painter.FadeTo(Image, 1, time);
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
            sceneChanging = false;
        }

        public void LoadScene(string sceneName)
        {
            LoadScene(sceneName, 2f);
        }

        public void LoadScene(string sceneName, float transitionTime)
        {
            lastTransitionTime = transitionTime;
            StartCoroutine(LoadInTime(sceneName, lastTransitionTime / 2));
        }
    }
}