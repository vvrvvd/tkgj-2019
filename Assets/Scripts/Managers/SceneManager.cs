﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class SceneManager : MonoBehaviour
    {
        private SceneFader Fader;

        private void Awake()
        {
            Fader = FindObjectOfType<SceneFader>();
        }

        public void LoadScene(string sceneName)
        {
            LoadScene(sceneName, 2f);
        }

        public void LoadScene(string sceneName, float time)
        {
            if (Fader) Fader.LoadScene(sceneName, time);
            else UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
        }
    }
}
