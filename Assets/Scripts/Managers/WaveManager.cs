﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    Crook_1,
    Crook_2,
    Villain,
    Boss
}

public class WaveManager : MonoBehaviour
{
    const int INITIAL_SPAWN_STRENGTH = 3;
    const int INITIAL_NUMBER_OF_SPAWNS = 3;
    const float INITIAL_TIME_BETWEEN_SPAWNS = 5f;

    public static WaveManager Instance;

    [SerializeField] private EnemySpawner[] spawnPoints;
    private EnemySpawner lastSpawnPoint;

    [Space]

    [SerializeField] private GameObject lvl1Crook_1;
    [SerializeField] private GameObject lvl1Crook_2;
    [SerializeField] private GameObject lvl35Villain;
    [SerializeField] private GameObject lvl100Boss;

    private List<EnemyType> availableEnemies = new List<EnemyType>();

    private int waveCount = 0;
    public int WaveCount => waveCount;
    private int spawnStrength;
    private int numberOfSpawns;
    private float timeBetweenSpawns;

    [SerializeField] private float timeBeforeFirstWave = 60f;
    [SerializeField] private float timeBetweenWaves = 20f;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = FindObjectOfType<WaveManager>();
            if(Instance != this)
                Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        spawnStrength = INITIAL_SPAWN_STRENGTH;
        numberOfSpawns = INITIAL_NUMBER_OF_SPAWNS;
        timeBetweenSpawns = INITIAL_TIME_BETWEEN_SPAWNS;

        if(Random.value < 0.5f)
            availableEnemies.Add(EnemyType.Crook_1);
        else
            availableEnemies.Add(EnemyType.Crook_2);

        StartCoroutine(StartSpawning());
    }

    private void OnEnable()
    {
        EnemySpawner.AllEnemiesDied.AddListener(OnWaveEnded);
    }

    private void OnDisable()
    {
        EnemySpawner.AllEnemiesDied.RemoveListener(OnWaveEnded);
    }

    private IEnumerator StartSpawning()
    {
        yield return new WaitForSeconds(timeBeforeFirstWave);
        StartCoroutine(SpawnNextWave());
    }

    private IEnumerator SpawnNextWave()
    {
        waveCount++;
        lastSpawnPoint = null;
        if (waveCount == 10)
        {
            EnemySpawner spawner = GetRandomSpawner();
            spawner.SpawnEnemies(lvl100Boss, 1);
            lastSpawnPoint = spawner;
        }
        else
        {
            for (int i = 0; i < numberOfSpawns; i++)
            {
                EnemySpawner spawner = GetRandomSpawner();
                GameObject enemy = null;
                switch (GetRandomEnemy())
                {
                    case EnemyType.Crook_1:
                        enemy = lvl1Crook_1;
                        break;
                    case EnemyType.Crook_2:
                        enemy = lvl1Crook_2;
                        break;
                    case EnemyType.Villain:
                        enemy = lvl35Villain;
                        break;
                    case EnemyType.Boss:
                        enemy = lvl100Boss;
                        break;
                }
                spawner.SpawnEnemies(enemy, spawnStrength / enemy.GetComponentInChildren<EnemyStats>().SpawnCost);
                lastSpawnPoint = spawner;
                yield return new WaitForSeconds(timeBetweenSpawns);
            }
        }
    }

    private EnemySpawner GetRandomSpawner()
    {
        List<EnemySpawner> availableSpawnPoints = new List<EnemySpawner>();
        availableSpawnPoints.AddRange(spawnPoints);
        if (lastSpawnPoint != null)
        {
            availableSpawnPoints.Remove(lastSpawnPoint);
        }
        float roll = Random.value;
        float value = 1f / (float)availableSpawnPoints.Count;
        for (int i = 0; i < availableSpawnPoints.Count; i++)
        {
            if (roll <= value)
                return availableSpawnPoints[i];
            value += 1f / (float)availableSpawnPoints.Count;
        }
        return availableSpawnPoints[0];
    }

    private EnemyType GetRandomEnemy()
    {
        float roll = Random.value;
        float value = 1f / (float)availableEnemies.Count;
        for (int i = 0; i < availableEnemies.Count; i++)
        {
            if (roll <= value)
                return availableEnemies[i];
            value += 1f / (float)availableEnemies.Count;
        }
        return availableEnemies[0];
    }

    private void IncreaseWaveStrength()
    {
        spawnStrength++;
        numberOfSpawns += waveCount % 2;
        timeBetweenSpawns *= 0.9f;

        if(waveCount == 3)
        {
            if (availableEnemies.Contains(EnemyType.Crook_2))
                availableEnemies.Add(EnemyType.Crook_1);
            else
                availableEnemies.Add(EnemyType.Crook_2);
        }
        if (waveCount == 5)
        {
            availableEnemies.Add(EnemyType.Villain);
        }
        if (waveCount == 10)
        {
            availableEnemies.Add(EnemyType.Boss);
        }
    }

    private void OnWaveEnded()
    {
        IncreaseWaveStrength();
        StartCoroutine(OnWaveEndedCoroutine());
    }

    private IEnumerator OnWaveEndedCoroutine()
    {
        yield return new WaitForSeconds(timeBetweenWaves);
        StartCoroutine(SpawnNextWave());
    }
}
