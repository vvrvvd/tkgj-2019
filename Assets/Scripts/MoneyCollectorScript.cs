﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MoneyCollectorScript
{
    public static int CurrentMoney { get; set; } = 0;

    public static void AddMoney(int money)
    {
        CurrentMoney += money;

    }
}
