﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatchBehaviour : MonoBehaviour, scr_IInteractable
{
    [SerializeField] private GameObject plantPrefab;
    private GameObject instaniatedPlant = null;


    private SelectorScript selector;
    private SpriteRenderer spriteRenderer;
    private scr_Player player;
    private ParticleSystem plantParticles;
    private ParticleSystem chemicalsParticles;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        selector = FindObjectOfType<SelectorScript>();
        player = FindObjectOfType<scr_Player>();
        plantParticles = transform.GetChild(1).GetComponent<ParticleSystem>();
        chemicalsParticles = transform.GetChild(2).GetComponent<ParticleSystem>();
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            LeftClick();
        }
        if (Input.GetMouseButtonDown(1))
        {
            RightClick();
        }
    }

    private void LeftClick()
    {
        if (instaniatedPlant != null)
        {
            instaniatedPlant.GetComponent<PlantBehaviour>().PickUpPlant();
        }
        else
        {
            instaniatedPlant = Instantiate(plantPrefab, transform.position, transform.rotation, transform);
        }
    }

    private void RightClick()
    {
        if (instaniatedPlant != null)
        {
            instaniatedPlant.GetComponent<PlantBehaviour>().MutatePlant();
        }
    }

    public void Use()
    {
        if (!player.isAttacking)
        {
            if (instaniatedPlant == null)
            {
                if (player.carriedItem is scr_Vegetable)
                {
                    var veg = (scr_Vegetable)player.carriedItem;
                    instaniatedPlant = Instantiate(veg.vegetablePrefab, transform.position, transform.rotation, transform);
                }
                else if (player.carriedItem is scr_ChemicalProbe)
                {
                    EmitChemicalsParticles();
                }
            }
            else
            {
                if (player.carriedItem is scr_ChemicalProbe)
                {
                    var plant = instaniatedPlant.GetComponent<PlantBehaviour>();

                    if (plant.CurrentStadium == PlantStadiums.Large)
                    {
                        EmitChemicalsParticles();
                        plant.MutatePlant();
                    }
                }
            }
        }
        else
        {
            if (instaniatedPlant!=null)
            {
                var plant = instaniatedPlant.GetComponent<PlantBehaviour>();
                plant.PickUpPlant();
                EmitPlantParticles();
            }
        }
    }

    public void Use(scr_IItem item)
    {
        if (!player.isAttacking)
        {
            if (instaniatedPlant == null)
            {

                if (player.carriedItem is scr_Vegetable)
                {
                    var veg = (scr_Vegetable)player.carriedItem;
                    instaniatedPlant = Instantiate(veg.vegetablePrefab, transform.position, transform.rotation, transform);
                    item.DestroyItem();
                    player.carriedItem = null;
                }
                else if (player.carriedItem is scr_ChemicalProbe)
                {
                    item.DestroyItem();
                    player.carriedItem = null;
                    EmitChemicalsParticles();
                }
            }
            else
            {
                if (player.carriedItem is scr_ChemicalProbe)
                {
                    var plant = instaniatedPlant.GetComponent<PlantBehaviour>();

                    if (plant.CurrentStadium == PlantStadiums.Large)
                    {
                        plant.MutatePlant();
                    }
                    else if (plant.CurrentStadium != PlantStadiums.Mutated)
                    {
                        plant.Die();
                    }

                    EmitChemicalsParticles();
                    item.DestroyItem();
                    player.carriedItem = null;
                }
            }
        }
        else
        {
            if (instaniatedPlant != null)
            {
                var plant = instaniatedPlant.GetComponent<PlantBehaviour>();
                plant.PickUpPlant();
                EmitPlantParticles();
            }

        }


    }

    public void Highlight()
    {
        selector.Select(spriteRenderer);
    }

    public void Unselect()
    {
        selector.Unselect();
    }

    public void EmitPlantParticles()
    {
        var count = UnityEngine.Random.Range(5, 10);
        plantParticles.Emit(count);
    }

    public void EmitChemicalsParticles()
    {
        var count = UnityEngine.Random.Range(4, 10);
        chemicalsParticles.Emit(count);
    }
}

