﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;

public class PlantBehaviour : Destroyable
{
    public GameObject[] PlantObjects;

    [SerializeField] protected float GrowingUpTime = 2f;
    [SerializeField] protected int MoneyForSecondStadium = 3;
    [SerializeField] protected int MoneyFoThirdStadium = 6;
    [SerializeField] private Material coinMaterial;
    [SerializeField] private Material mutateMaterial;
    [SerializeField] private Material seedMaterial;
    [SerializeField] private Material cutMaterial;

    public PlantStadiums CurrentStadium;

    protected int currentStadiumIndex = 0;
    protected GameObject plantObject;
    protected ParticleSystem patchParticleSystem;
    protected GameManager GameManager;

    protected bool isDying = false;

    protected virtual void Awake()
    {
        PlantObjects = new GameObject[4];
        for (int i = 0; i < 4; i++)
        {
            PlantObjects[i] = transform.GetChild(i).gameObject;
            PlantObjects[i].SetActive(false);
        }

        GameManager = FindObjectOfType<GameManager>();
        var drawnManager = FindObjectOfType<scr_DrawnManager>();
        drawnManager.AddDrawnObject(GetComponent<DrawingSorter>());
    }

    protected virtual void Start()
    {
        CurrentStadium = PlantStadiums.Small;
        PlantObjects[(int)CurrentStadium].SetActive(true);

        patchParticleSystem = transform.parent.Find("Particle_System").GetComponent<ParticleSystem>();

        plantObject = PlantObjects[0];
        ExecuteParticleSystem(seedMaterial);
        GrowCycle();

        Death.AddListener(() =>
        {
            GetComponent<Animator>().SetTrigger("Death");
            isDying = true;
        });
    }

    protected void OnDestroy()
    {
        if (CurrentStadium == PlantStadiums.Medium)
            GameManager.GiveGold(MoneyForSecondStadium);
        else if (CurrentStadium == PlantStadiums.Large)
            GameManager.GiveGold(MoneyFoThirdStadium);
    }
    protected virtual void FixedUpdate()
    {
        if (isDying) return;

        Attack();
    }

    private void GrowCycle()
    {
        if (!CurrentStadium.Equals(PlantStadiums.Large) && !CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            StartCoroutine(Grow());
        }        
    }

    private IEnumerator Grow()
    {
        yield return new WaitForSeconds(GrowingUpTime);
        if (!isDying)
        {
            currentStadiumIndex++;
            CurrentStadium = (PlantStadiums)currentStadiumIndex;
            SetProperState();
            GrowCycle();
        }
    }

    public void PickUpPlant()
    {
        if (CurrentStadium.Equals(PlantStadiums.Large))
        {
            MoneyCollectorScript.AddMoney(MoneyFoThirdStadium);
            ExecuteParticleSystem(coinMaterial);

        }
        else if (CurrentStadium.Equals(PlantStadiums.Medium))
        {
            MoneyCollectorScript.AddMoney(MoneyForSecondStadium);
            ExecuteParticleSystem(coinMaterial);

        }
        else
        {
            ExecuteParticleSystem(cutMaterial);

        }

        Destroy(gameObject);     
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    public void MutatePlant()
    {
        if (CurrentStadium.Equals(PlantStadiums.Large))
        {
            currentStadiumIndex = (int)PlantStadiums.Mutated;
            CurrentStadium = PlantStadiums.Mutated;
            ExecuteParticleSystem(mutateMaterial);
            SetProperState();
        }
    }

    private void SetProperState()
    {
        for (int i = 0; i < 4; i++)
            PlantObjects[i].SetActive(false);

        PlantObjects[(int)CurrentStadium].SetActive(true);
    }

    private void ExecuteParticleSystem(Material material)
    {
        var main = patchParticleSystem.main;
        if (material == seedMaterial || material == cutMaterial)
            main.startSize = 1.75f;
        else
            main.startSize = 1.0f;

        patchParticleSystem.GetComponent<ParticleSystemRenderer>().material = material;
        patchParticleSystem.Emit(1);
    }

    protected virtual void Attack()
    {

    }

    public void VanishAnimation()
    {

    }
}

public enum PlantStadiums
{
    Small = 0,
    Medium = 1,
    Large = 2,
    Mutated = 3
}

