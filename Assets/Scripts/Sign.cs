﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sign : MonoBehaviour
{
    [TextArea]
    public string tooltip;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        FindObjectOfType<UIDialogue>().OpenDialogue(tooltip);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        FindObjectOfType<UIDialogue>().CloseDialogue();
    }
}
