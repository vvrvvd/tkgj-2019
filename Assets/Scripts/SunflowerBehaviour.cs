﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunflowerBehaviour : PlantBehaviour
{
    // Strzela laserami - obrażenia ciągłe
    [SerializeField] private int damage = 4;
    [SerializeField] private Material laserMaterial;
    [SerializeField] private float laserWidthSpeed = 0.1f;
    [SerializeField] private float laserDesiredWidth = 1f;
    [SerializeField] private Transform facePivot;

    private ParticleSystem aimParticleSystem;
    private ParticleSystem sourceParticleSystem;


    private List<EnemyStats> enemiesInRange;
    private EnemyStats nearestEnemy;

    private LineRenderer laserLineRenderer;

    protected override void Awake()
    {
        base.Awake();
        enemiesInRange = new List<EnemyStats>();
        laserLineRenderer = transform.Find("Laser").GetComponent<LineRenderer>();
        laserLineRenderer.SetPosition(0, transform.position);
        laserLineRenderer.material = laserMaterial;
        laserLineRenderer.enabled = false;

        aimParticleSystem = transform.Find("FlareParticle Aim").GetComponent<ParticleSystem>();
        sourceParticleSystem = transform.Find("FlareParticle Source").GetComponent<ParticleSystem>();
        AbleParticles(false);
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (!isDying) return;
        RemoveDestroyedEnemiesFromList();
        if(!nearestEnemy)
        {
            StartCoroutine(EreaseLaser());
            SetNearestEnemy();
        }
    }

    protected override void Attack()
    {
        if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            if (nearestEnemy)
            {
                nearestEnemy.GiveDamage(damage);
                DrawLaser(nearestEnemy.transform.position);
            }
            else
            {
                StartCoroutine(EreaseLaser());
            }
        }
    }

    #region LaserAndParticles

    private void DrawLaser(Vector2 end)
    {
        laserLineRenderer.enabled = true;
        laserLineRenderer.SetPosition(0, facePivot.position);
        laserLineRenderer.SetPosition(1, end);
        StartCoroutine(IcreaseLaserWidth());
        aimParticleSystem.transform.position = end;
        sourceParticleSystem.transform.position = facePivot.position;
    }

    private IEnumerator IcreaseLaserWidth()
    {
        while(laserLineRenderer.startWidth<laserDesiredWidth)
        {
            laserLineRenderer.startWidth += laserWidthSpeed;
            laserLineRenderer.endWidth += laserWidthSpeed;
            yield return null;
        }

        AbleParticles(true);

    }

    private IEnumerator EreaseLaser()
    {
        while (laserLineRenderer.startWidth >= 0)
        {
            laserLineRenderer.startWidth -= laserWidthSpeed;
            laserLineRenderer.endWidth -= laserWidthSpeed;
            yield return null;
        }

        laserLineRenderer.enabled = false;
        AbleParticles(false);
    }

    private void AbleParticles(bool enable)
    {
        ParticleSystem.EmissionModule emAim = aimParticleSystem.emission;
        ParticleSystem.EmissionModule emSrc = sourceParticleSystem.emission;
        emAim.enabled = enable;
        emSrc.enabled = enable;
    }

    #endregion

    private void SetNearestEnemy()
    {
        float nearestDistance = -1;
        int indexOfNearest = -1;
        for (int i = 0; i < enemiesInRange.Count; i++)
        {
            float magnitude = (transform.position - enemiesInRange[i].transform.position).magnitude;
            if (i == 0)
            {
                nearestDistance = magnitude;
                indexOfNearest = 0;
            }
            else
            {
                if (magnitude < nearestDistance)
                {
                    nearestDistance = magnitude;
                    indexOfNearest = i;
                }
            }

        }
        if (indexOfNearest == -1)
            nearestEnemy = null;
        else
            nearestEnemy = enemiesInRange[indexOfNearest];
    }

    private void RemoveDestroyedEnemiesFromList()
    {
        for (int i = 0; i < enemiesInRange.Count; i++)
        {
            if (enemiesInRange[i] == null)
                enemiesInRange.Remove(enemiesInRange[i]);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            var destruct = collision.GetComponent<EnemyStats>();
            if (destruct)
            {
                enemiesInRange.Add(destruct);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            var destruct = collision.GetComponent<EnemyStats>();
            if (destruct)
            {
                enemiesInRange.Remove(destruct);
                if(destruct == nearestEnemy)
                {
                    nearestEnemy = null;
                }
            }
        }
    }
}
