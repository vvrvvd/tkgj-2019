﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TestEnemyControllerMarta : Destroyable
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private GameObject vines;

    private Rigidbody2D playerRigidbody;
    bool stuck = false;

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (!stuck)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                playerRigidbody.velocity = Vector2.up * speed;
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                playerRigidbody.velocity = Vector2.down * speed;

            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                playerRigidbody.velocity = Vector2.right * speed;

            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                playerRigidbody.velocity = Vector2.left * speed;

            }

        }
    

    }

    public void Stuck()
    {
        if (!stuck)
        {
            playerRigidbody.velocity = Vector2.zero;
            GameObject v = Instantiate(vines);
            v.transform.position = transform.position;
        }
        stuck = true;

    }
}
