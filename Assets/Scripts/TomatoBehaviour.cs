﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TomatoBehaviour : PlantBehaviour
{
    [SerializeField] private int maxEnemiesAttacked = 3;

    [Space]

    public List<EnemyStats> enemiesInRange;
    public List<EnemyStats> attackedEnemies;

    protected override void Start()
    {
        base.Start();
        enemiesInRange = new List<EnemyStats>();
        attackedEnemies = new List<EnemyStats>();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (!CurrentStadium.Equals(PlantStadiums.Mutated)) return;
        RemoveDestroyedEnemiesFromList();
       
        if (attackedEnemies.Count < maxEnemiesAttacked)
        {
            AddNewToNearestEnemies();
        }
    }

    protected override void Attack()
    {
        for (int i = 0; i < attackedEnemies.Count; i++)
        {
            float baseSpeed = attackedEnemies[i].speed;
            attackedEnemies[i].GetComponent<NavMeshAgent>().speed = baseSpeed / 2.0f;
        }
    }

    private void AddNewToNearestEnemies()
    {
        float nearestDistance = int.MaxValue;
        int indexOfNearest = -1;

        for (int i = 0; i < enemiesInRange.Count; i++)
        {
            float magnitude = (transform.position - enemiesInRange[i].transform.position).magnitude;

            if (magnitude < nearestDistance && !attackedEnemies.Contains(enemiesInRange[i]))
            {
                nearestDistance = magnitude;
                indexOfNearest = i;
            }
        }

        if (indexOfNearest != -1)
        {
            attackedEnemies.Add(enemiesInRange[indexOfNearest]);
        }
    }

    private void RemoveDestroyedEnemiesFromList()
    {
        attackedEnemies.RemoveAll(item => item == null);
        enemiesInRange.RemoveAll(item => item == null);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            var destruct = collision.GetComponent<EnemyStats>();
            if (destruct)
            {
                enemiesInRange.Add(destruct);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //if (CurrentStadium.Equals(PlantStadiums.Mutated))
        {
            var destruct = collision.GetComponent<EnemyStats>();
            if (destruct)
            {
                enemiesInRange.Remove(destruct);
                if (attackedEnemies.Contains(destruct))
                {
                    attackedEnemies.Remove(destruct);
                }
            }
        }
    }
}
