﻿using System.Collections;
using TMPro;
using UnityEngine;

public class UIDialogue : MonoBehaviour
{
    private bool isDialogueOpen;
    private Coroutine dialoguePrinter;

    [SerializeField] private GameObject dialogueBox;
    [SerializeField] private TextMeshProUGUI dialogueText;

    public void OpenDialogue(string text)
    {
        dialogueBox.SetActive(true);
        if (dialoguePrinter != null)
            StopCoroutine(dialoguePrinter);
        dialoguePrinter = StartCoroutine(PrintDialogue(text));
        isDialogueOpen = true;
    }

    public void CloseDialogue()
    {
        dialogueBox.SetActive(false);
        isDialogueOpen = false;
    }

    public IEnumerator PrintDialogue(string text)
    {
        dialogueText.text = "";
        for (int i = 0; i < text.Length; i++)
        {
            dialogueText.text += text[i];
            yield return new WaitForSeconds(0.05f);
        }
        dialoguePrinter = null;
    }
}
