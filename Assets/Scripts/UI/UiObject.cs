﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UiObject : MonoBehaviour
    {
        #region Fields

        private CanvasGroup CanvasGroup;

        public float fadingTime = 0.5f;

        #endregion

        #region Init and deinit methods

        private void Awake()
        {
            #region Self-Injection

            CanvasGroup = GetComponent<CanvasGroup>();

            #endregion

            StartCoroutine(Painter.FadeTo(CanvasGroup, 0, 0));
        }

        private void OnEnable()
        {
            StartCoroutine(Painter.FadeTo(CanvasGroup, 1, fadingTime));
        }

        private IEnumerator DisableInTime()
        {     
            yield return Painter.FadeTo(CanvasGroup, 0, fadingTime);
            Active = false;
        }

        public void Disable()
        {
            if (!Active) return;
            CanvasGroup.blocksRaycasts = false;
            StartCoroutine(DisableInTime());
        }

        public void Enable()
        {
            CanvasGroup.blocksRaycasts = false;
            Active = true;
        }

        public void Toggle()
        {
            if (Active)
                Disable();
            else
                Enable();
        }

        #endregion

        #region Properties

        public bool Active
        {
            get { return gameObject.activeSelf; }
            set { gameObject.SetActive(value); }
        }

        #endregion
    }
}
