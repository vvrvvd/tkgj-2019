﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public static class Painter
    {
        public static void SetMaterialTransparent(Renderer Mesh)
        {
            foreach (Material m in Mesh.materials)
            {
                m.SetFloat("_Mode", 2);
                m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                m.SetInt("_ZWrite", 0);
                m.DisableKeyword("_ALPHATEST_ON");
                m.EnableKeyword("_ALPHABLEND_ON");
                m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                m.renderQueue = 3000;
            }
        }

        public static void SetMaterialOpaque(Renderer Mesh)
        {
            foreach (Material m in Mesh.materials)
            {
                m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                m.SetInt("_ZWrite", 1);
                m.DisableKeyword("_ALPHATEST_ON");
                m.DisableKeyword("_ALPHABLEND_ON");
                m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                m.renderQueue = -1;
            }
        }

        public static void SetEmission(Renderer mesh, Color color)
        {
            MaterialPropertyBlock block = new MaterialPropertyBlock();
            mesh.GetPropertyBlock(block);
            block.SetColor("_OverlayColor", color);
            mesh.SetPropertyBlock(block);

            //foreach (var mat in mesh.materials)
            //{
            //    //mat.SetColor("_OverlayColor", Color.blue);
            //    //mat.EnableKeyword("_OVERLAY_ON");
            //    //mat.EnableKeyword("_EMISSION");
            //}
        }

        public static void ResetEmission(Renderer mesh, Color color)
        {
            MaterialPropertyBlock block = new MaterialPropertyBlock();
            mesh.GetPropertyBlock(block);
            block.SetColor("_OverlayColor", color);
            mesh.SetPropertyBlock(block);

            //foreach (var mat in mesh.materials)
            //{

            //    //mat.SetColor("_OverlayColor", Color.white);
            //    //mat.DisableKeyword("_OVERLAY_ON");
            //    //mat.DisableKeyword("_EMISSION");
            //}
        }

        public static IEnumerator FadeTo(CanvasGroup group, float alpha, float time = 0.5f)
        {
            var startAlpha = group.alpha;
            var elapsed = 0.0f;

            while (elapsed < time)
            {
                group.alpha = Mathf.Lerp(startAlpha, alpha, elapsed / time);
                elapsed += Time.unscaledDeltaTime;
                yield return null;
            }

            group.alpha = alpha;
        }

        public static IEnumerator FadeTo(Image image, float alpha, float time = 0.5f)
        {
            var color = image.color;
            var startAlpha = color.a;
            var elapsed = 0.0f;

            while (elapsed < time)
            {
                color.a = Mathf.Lerp(startAlpha, alpha, elapsed / time);
                image.color = color;
                elapsed += Time.unscaledDeltaTime;
                yield return null;
            }

            color.a  = alpha;
            image.color = color;
        }

        public static IEnumerator FadeTo(SpriteRenderer sprite, float alpha, float time = 0.5f)
        {
            var color = sprite.color;
            var startAlpha = color.a;
            var elapsed = 0.0f;

            while (elapsed < time)
            {
                color.a = Mathf.Lerp(startAlpha, alpha, elapsed / time);
                sprite.color = color;
                elapsed += Time.unscaledDeltaTime;
                yield return null;
            }

            color.a = alpha;
            sprite.color = color;
        }
    }
}