﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;

public class scr_Barrel : MonoBehaviour, scr_IInteractable
{
    public int goldCost;
    public GameObject seed;
    public ParticleSystem NoMoneyParticle;
    public scr_PriceCloud cloud;

    private scr_IItem item;
    private GameObject PlayerObject;
    private scr_Player player;
    private SelectorScript selector;
    private SpriteRenderer spriteRenderer;
    private GameManager GameManager;

    // Start is called before the first frame update
    void Start()
    {
        GameManager = FindObjectOfType<GameManager>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = GameObject.Find("Player").GetComponent<scr_Player>();
        selector = GameObject.FindGameObjectWithTag("Selector").GetComponent<SelectorScript>();
        item = Instantiate(seed).GetComponent<scr_IItem>();
        item.Hide();


        if (cloud) cloud.SetText(goldCost.ToString());
    }


    public void Use()
    {
        if (!GameManager.UseGold(goldCost))
        {
            if (!NoMoneyParticle) return;
            NoMoneyParticle.Play();
            return;
        }

        player.TakeItem(item);
        selector.Unselect();
    }

    public void Use(scr_IItem item)
    {
        Use();
    }

    public void Highlight()
    {
        if (cloud) cloud.SetActive(true);
        selector.Select(spriteRenderer);
    }

    public void Unselect()
    {
        if (cloud) cloud.SetActive(false);
        selector.Unselect();
    }

    
}
