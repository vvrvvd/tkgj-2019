﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ChemicalProbe : MonoBehaviour, scr_IItem
{
    private SpriteRenderer player;
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        player = GameObject.Find("Player").GetComponentInChildren<SpriteRenderer>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected virtual void Update()
    {

    }

    protected virtual void LateUpdate()
    {
        spriteRenderer.sortingOrder = player.sortingOrder + 1;
    }

    public void SetPlayerOffset(Vector3 offset)
    {
        transform.localPosition = offset;
    }

    public GameObject InstantiateItem(Transform parent)
    {
        var obj = Instantiate(this.gameObject, parent);
        obj.GetComponent<scr_IItem>().Show();
        return obj;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void DestroyItem()
    {
        Destroy(gameObject);
    }
}
