﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_DrawnManager : MonoBehaviour
{
    public List<DrawingSorter> drawnObjects;

    // Start is called before the first frame update
    void Start()
    {
        drawnObjects = new List<DrawingSorter>(FindObjectsOfType<DrawingSorter>());
        SortByYPos();
    }

    // Update is called once per frame
    void Update()
    {
        DeleteNulls();
        SortByYPos();
        
        foreach (DrawingSorter o in drawnObjects)
        {
            for(int i=0; i < o.sprRenderer.Length; i++)
            {
                o.sprRenderer[i].sortingOrder = drawnObjects.IndexOf(o);
            }
        }
    }

    private void SortByYPos()
    {
        drawnObjects.Sort((ob1, ob2) => (ob2.gameObject.transform.position.y.CompareTo((ob1.gameObject.transform.position.y))));
    }

    private void DeleteNulls()
    {
      drawnObjects.RemoveAll(drawn => drawn == null);
    }

    public void AddDrawnObject(DrawingSorter obj)
    {
        drawnObjects.Add(obj);
    }


}
