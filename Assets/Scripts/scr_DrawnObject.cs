﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface scr_DrawnObject 
{
    SpriteRenderer GetSpriteRenderer();
}
