﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface scr_IInteractable
{
    void Use();
    void Use(scr_IItem item);
    void Highlight();
    void Unselect();
}
