﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface scr_IItem
{
    void Hide();
    void Show();
    void DestroyItem();
    void SetPlayerOffset(Vector3 offset);
    GameObject InstantiateItem(Transform parent);
}
