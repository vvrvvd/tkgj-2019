﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class scr_Player : MonoBehaviour {
    enum faceDirection {Up, Left, Down, Right};

    public bool isFacingLeft = false, isWalking = false, isHolding = false;
    public Animator slashAnimator;

    public float speed = 2f;
    public int facing = 2;
    public float interactingRange = 1f;
    public scr_IItem carriedItem;
    public Vector3 carriedItemOffsetRight = Vector3.zero;
    public Vector3 carriedItemOffsetLeft = Vector3.zero;
    public Vector3 carriedItemOffsetDown = Vector3.zero;
    public Vector3 carriedItemOffsetUp = Vector3.zero;
    public SpriteRenderer actionButton;
    private bool isUsingAction;
    private int oldFacing;
    private Rigidbody2D rb;
    private Collider2D coll;
    private HashSet<scr_IInteractable> interactablesInRange;
    private scr_IInteractable detectedInteractive;
    private Vector2 movDirection = Vector2.zero;
    private Animator animator;
    private float attackCooldown;
    public bool isAttacking = false;

    // Start is called before the first frame update
    void Start()
    {
        interactablesInRange = new HashSet<scr_IInteractable>();
        animator = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody2D>();
        coll = GetComponent<Collider2D>();
        actionButton.enabled = false;
        attackCooldown = AnimationLength("AttackSlash")*1.6f;
        slashAnimator.transform.parent.gameObject.SetActive(false);
    }

    float AnimationLength(string name)
    {
        float time = 0;
        RuntimeAnimatorController ac = slashAnimator.runtimeAnimatorController;

        for (int i = 0; i < ac.animationClips.Length; i++)
            if (ac.animationClips[i].name == name)
                time = ac.animationClips[i].length;

        return time;
    }


    // Update is called once per frame
    void Update()
    {
        if (!isAttacking)
        {
            movDirection = GetWalkInput();
            isUsingAction = GetUsingActionInput();
            setFacing(movDirection);

            if (detectedInteractive != null && isUsingAction)
            {
                detectedInteractive.Use(carriedItem);
                if (carriedItem == null)
                {
                    isHolding = false;
                }
            }

            if (Input.GetKey(KeyCode.Space) || Input.GetButtonDown("Fire3") || Input.GetButtonDown("Jump"))
            {
                if (carriedItem != null)
                    carriedItem.Hide();
                isAttacking = true;
                animator.SetBool("isAttacking", true);
                slashAnimator.ResetTrigger("ComeBack");
                slashAnimator.SetTrigger("Attack");
                Invoke("AttackCooldown", attackCooldown);
                movDirection = Vector2.zero;
                if(detectedInteractive!=null)
                    detectedInteractive.Use();
            }

            UpdateAnimation();
        }
    }

    private void AttackCooldown()
    {
        isAttacking = false;
        if(carriedItem != null)
            carriedItem.Show();
        slashAnimator.ResetTrigger("Attack");
        slashAnimator.SetTrigger("ComeBack");
    }

    private void FixedUpdate()
    {
        Walk(movDirection, speed);
    }

    private Vector2 GetWalkInput()
    {
        float xAxis = 0, yAxis = 0;
        xAxis = Input.GetAxisRaw("Horizontal");
        yAxis = Input.GetAxisRaw("Vertical");
        return new Vector2(xAxis, yAxis);
    }

    private bool GetUsingActionInput()
    {
        return (Input.GetKeyDown(KeyCode.F) || Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2"));
    }

    // Debugowe
    private void DropItem()
    {

    }

    public void TakeItem(scr_IItem item)
    {
        if (carriedItem != null)
        {
            carriedItem.DestroyItem();
        }
        carriedItem = item.InstantiateItem(transform).GetComponent<scr_IItem>();
        carriedItem.SetPlayerOffset(ChooseCarriedItemOffset());
        isHolding = true;
    }

    private void Walk(Vector2 movDirection, float speed)
    {
        rb.velocity = movDirection.normalized * speed;
        if (rb.velocity.magnitude != 0)
            isWalking = true;
        else
            isWalking = false;
    }

    private void setFacing(Vector2 movDirection)
    {

        if (movDirection.x >= Mathf.Abs(movDirection.y) && movDirection.x > 0)
            facing = (int)faceDirection.Right;
        else if (movDirection.x <= Mathf.Abs(movDirection.y) && movDirection.x < 0)
            facing = (int)faceDirection.Left;
        else if (movDirection.y > Mathf.Abs(movDirection.x) && movDirection.y > 0)
            facing = (int)faceDirection.Up;
        else if (movDirection.y < Mathf.Abs(movDirection.x) && movDirection.y < 0)
            facing = (int)faceDirection.Down;

        if (movDirection.x < 0)
        {
            if (carriedItem != null && !isFacingLeft)
            {
                isFacingLeft = true;
                carriedItem.SetPlayerOffset(ChooseCarriedItemOffset());
            }
            else
                isFacingLeft = true;
        }
        else if (movDirection.x > 0)
        {
            if (carriedItem != null && isFacingLeft)
            {
                isFacingLeft = false;
                carriedItem.SetPlayerOffset(ChooseCarriedItemOffset());
            }
            else

                isFacingLeft = false;
        }

        oldFacing = facing;
    }

    private Collider2D DetectInteractives(float range, int facing)
    {
        Vector2 detectDirection = new Vector2(range, 0);
        switch (facing)
        {
            case (int) faceDirection.Down:
                detectDirection = new Vector2(0, -range);
                break;
            case (int)faceDirection.Left:
                detectDirection = new Vector2(-range, 0);
                break;
            case (int)faceDirection.Up:
                detectDirection = new Vector2(0, range);
                break;
            case (int)faceDirection.Right:
                detectDirection = new Vector2(range, 0);
                break;
        }

        RaycastHit2D[] hit = new RaycastHit2D[1];
        coll.Raycast(detectDirection, hit, range);
        Debug.DrawLine(transform.position, transform.position + new Vector3(detectDirection.x, detectDirection.y, 0) * range, Color.green);
        actionButton.enabled = false;

        if (hit[0].collider != null)
        {
            if (hit[0].collider.gameObject.GetComponent(typeof(scr_IInteractable)) != null)
            {
                Debug.DrawLine(transform.position, transform.position + new Vector3(detectDirection.x, detectDirection.y, 0) * range, Color.green);
                actionButton.enabled = true;
                return hit[0].collider;
            }
            else
            {
                Debug.DrawLine(transform.position, transform.position + new Vector3(detectDirection.x, detectDirection.y, 0) * range, Color.red);
               
                return null;
            }
        }
        else
            return null;
    }

    private Vector3 ChooseCarriedItemOffset()
    {
        if (isFacingLeft)
            return carriedItemOffsetLeft;
        else
            return carriedItemOffsetRight;
    }

    private void UpdateAnimation()
    {
        animator.SetBool("isWalking", isWalking);
        animator.SetBool("isFacingLeft", isFacingLeft);
        animator.SetBool("isHolding", isHolding);
        animator.SetBool("isAttacking", isAttacking);
    }

    protected void OnTriggerEnter2D(Collider2D coll)
    {
        var interactable = coll.GetComponent<scr_IInteractable>();
        if (interactable != null)
        {
            interactablesInRange.Add(interactable);
            detectedInteractive = interactablesInRange.OrderBy(i => Vector3.Distance(transform.position, coll.transform.position)).First();
            SelectInteractable();
        }
    }

    protected void OnTriggerExit2D(Collider2D coll)
    {
        var interactable = coll.GetComponent<scr_IInteractable>();
        if (interactable != null && interactablesInRange.Contains(interactable))
        {
            interactablesInRange.Remove(interactable);
            if (interactable == detectedInteractive)
            {
                if(interactablesInRange.Count==0)
                {
                    UnselectInteractable();
                    detectedInteractive = null;
                }
                else
                {
                    detectedInteractive = interactablesInRange.OrderBy(i => Vector3.Distance(transform.position, coll.transform.position)).First();
                    SelectInteractable();
                }

            }
        }
    }

    private void SelectInteractable()
    {
        detectedInteractive.Highlight();
        actionButton.enabled = true;
    }

    private void UnselectInteractable()
    {
        actionButton.enabled = false;
        detectedInteractive.Unselect();
    }

}
