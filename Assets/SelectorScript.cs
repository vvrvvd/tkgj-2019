﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorScript : MonoBehaviour
{

    public float offsetY;
    public float scaleMovement;

    private Transform up;
    private Transform down;
    private SpriteRenderer upRender;
    private SpriteRenderer downRender;

    private Vector3 startUpPosition;
    private Vector3 startDownPosition;

    void Awake()
    {
        up = transform.GetChild(0);
        down = transform.GetChild(1);
        upRender = up.GetComponent<SpriteRenderer>();
        downRender = down.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        up.transform.localPosition = startUpPosition + Vector3.up * Mathf.Sin(Time.time) * scaleMovement * Time.deltaTime;
        down.transform.localPosition = startDownPosition - Vector3.up * Mathf.Sin(Time.time) * scaleMovement * Time.deltaTime;
    }

    public void Select(SpriteRenderer target)
    {
        transform.position = target.bounds.center;
        up.localPosition = new Vector3(up.localPosition.x, target.bounds.size.y / 2.0f + offsetY, up.localPosition.z);
        down.localPosition = new Vector3(down.localPosition.x, -target.bounds.size.y / 2.0f - offsetY, down.localPosition.z);
        startUpPosition = up.localPosition;
        startDownPosition = down.localPosition;
        float width = target.bounds.size.x / upRender.size.x;
        up.localScale = new Vector3(width, width, 1.0f);
        down.localScale = new Vector3(width, width, 1.0f);
        up.gameObject.SetActive(true);
        down.gameObject.SetActive(true);
    }

    public void Unselect()
    {
        up.gameObject.SetActive(false);
        down.gameObject.SetActive(false);
    }
}
