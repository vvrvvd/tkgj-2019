﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashAttackAnimator : MonoBehaviour
{

    [SerializeField] private Animator slashAnimator;

    public void PlayAttack()
    {
        slashAnimator.ResetTrigger("ComeBack");
        slashAnimator.SetTrigger("Attack");
    }

    public void StopAttack()
    {
        slashAnimator.ResetTrigger("Attack");
        slashAnimator.SetTrigger("ComeBack");
    }

}
