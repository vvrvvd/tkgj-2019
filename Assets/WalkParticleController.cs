﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkParticleController : MonoBehaviour
{

    public ParticleSystem walkParticles;

    public void EmitWalkParticles()
    {
        var count = Random.Range(3, 6);
        walkParticles.Emit(count);
    }
}
