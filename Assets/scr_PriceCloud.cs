﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Game;

[RequireComponent(typeof(SpriteRenderer))]
public class scr_PriceCloud : MonoBehaviour
{
    private SpriteRenderer IconRenderer;
    private SpriteRenderer SpriteRenderer;
    private TextMeshPro Text;
    // Start is called before the first frame update
    void Awake()
    { 
        IconRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        Text = transform.GetChild(1).GetComponent<TextMeshPro>();
        SpriteRenderer = GetComponent<SpriteRenderer>();
        SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3 (0, Mathf.Sin((Time.time + Mathf.PI*0.5f) * 4f)*0.002f, 0);
    }

    public void SetText(string text)
    {
        Text.SetText(text);
    }

    public void SetActive(bool value)
    {
        Text.enabled = value;

        StartCoroutine(Painter.FadeTo(SpriteRenderer, value ? 1 : 0, 0.5f));
        StartCoroutine(Painter.FadeTo(IconRenderer, value ? 1 : 0, 0.5f));
    }
}
